#!/usr/bin/perl -w

use CGI qw/:all/;
use CGI::Carp qw(fatalsToBrowser warningsToBrowser);
use Data::Dumper; 
use POSIX qw(strftime);
use POSIX qw(tzset);
use Encode;
$ENV{TZ} = 'Australia/Sydney';
tzset;
warningsToBrowser(1);
if ($ENV{REQUEST_URI} =~ /log/) {
	print header('-charset'=>'utf-8'),start_html('-lang'=>"zh-CN", '-title'=>'What we ate?') ,h1(b('吃了什么？日志查看')), "\n";
	$enable = 0;
}
else { 
	print header('-charset'=>'utf-8'),start_html('-lang'=>"zh-CN", '-title'=>'What we eat today?') ,h1(b('吃什么？随机结果:')), "\n";
	$enable = 1;
}
$now = strftime "%Y/%m/%d  (%A)   %T  %Z", localtime;
print p, h2('餐馆:'), p if ($enable);
$random1 = int(rand(6));
$choose1 = '香满堂' if ($random1 == 0);
$choose1 = '上海旺旺' if ($random1 == 1);
$choose1 = '天府飘香' if ($random1 == 2);
$choose1 = '大吉手工面馆' if ($random1 == 3);
$choose1 = '鼎膳' if ($random1 == 4);
$choose1 = '煲仔饭' if ($random1 == 5);
print h3($choose1) if ($enable);
print p, h2('类型:'), p if ($enable);
$random2 = int(rand(2));
$choose2 = '饭类' if ($random2 == 0);
$choose2 = '点菜' if ($random2 == 1);
print h3($choose2) if ($enable);
print '<br>现在时间: ' , $now;
if ($enable) {
	open(FH, '>>', 'eat.txt') or die "can not open eat.txt: $!";
	print FH "$now\t\t$choose1\t\t$choose2\n";
	close FH;
	open(FR, '<', 'eat.txt') or die "can not open eat.txt: $!";
	@lines = <FR>;
	$dp = $#lines;
	$dp++;
	$dp = 10 if $dp > 10;
	print '<br>--------------------<br>';
	print "<b>今日最近 $dp 条历史记录：</b><br>";
	$today = strftime "%Y/%m/%d", localtime;
	for ($i  = $#lines; $i >= $#lines - 9 && $i >= 0; $i--)  {
		$lines[$i] =~ s/\t\t/,/g;
		if ($lines[$i] =~ /$today/) {
			print "$lines[$i]<br>";
		}
	}
	close(FR);
	print '--------------------<br>';
	print '<b>记住本页的网址是：<a href="http://eat.sonicz.com/">http://eat.sonicz.com/</a>或<a href="http://sonic.host-ed.me/sonic/cgi-bin/eat.cgi">http://sonic.host-ed.me/sonic/cgi-bin/eat.cgi</a><br>查询：<a href="http://sonic.host-ed.me/sonic/cgi-bin/eat.cgi?log">http://sonic.host-ed.me/sonic/cgi-bin/eat.cgi?log</a><br></b>';
}
else {
	print '<br>--------------------<br>';
	open(FR, '<', 'eat.txt') or die "can not open eat.txt: $!";
	@lines = <FR>;
	$dp = $#lines;
	$dp++;
	print "<b>总共 $dp 条历史记录：</b><br>";
	foreach $line (@lines)  {
		$line =~ s/\t\t/,/g;
		print "$line<br>";
	}
	close(FR);
}
print '<br>--------------------<br>';
print small('随机选餐系统 1.8版, Zeke制作, Perl CGI, 2014年11月06日.<br>Source code: <a href="https://bitbucket.org/ziyizhou/eat" target=\"_blank\">https://bitbucket.org/ziyizhou/eat</a>');
exit(0);